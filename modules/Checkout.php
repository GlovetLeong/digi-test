<?php
require_once ('models/Product.php');

class Checkout
{
	private $pricing_rule = [];
	private $product = [];
	private $final_scan_result = [];

	function __construct($pricing_rule = [])
	{
		$this->pricing_rule = $pricing_rule;
		$product = new Product();
		$this->product = $product
					->where('status', 1)
					->get();
	}

	function getProductPrice($sku)
	{
		$product = [];
		foreach ($this->product as $key => $row) {
			if ($sku == $row->sku) {
				$product = $row;
			}
		}
		return $product;
	}

	private function classItem($item)
	{
		$class_item = [];
		foreach ($item as $key_class => $row_class) {
			if (empty($class_item[$row_class])) {
				$class_item[$row_class] = 1;
			}
			else {
				$class_item[$row_class] += 1;
			}
		}
		return $class_item;
	}

	private function scanResult($class_item)
	{
		$scan_result = [];
		foreach ($class_item as $key_scan => $row_scan) {
			$product = $this->getProductPrice($key_scan);
			$scan_result[$key_scan] = (object)[
				'sku' => $key_scan,
				'quantity' => $row_scan,
				'price' => $product->price,
				'total' => $product->price * $row_scan
			];
		}
		return $scan_result;
	}

	private function quaterCalculate($quantity = 0, $rule_unit = 0, $free_unit = 0, $history = [], $type = '')
	{
		$remain = $remain_unit = $quantity - $rule_unit;
		if ($type == 'same') {
			$remain = $remain_free = $remain_unit - $free_unit;
		}
		$pay = !empty($history) ? $history->pay + ($remain_unit > 0 ? $rule_unit : $quantity) : $rule_unit;
		$free = !empty($history) ? $history->free + ($remain_unit >= 0 ? $free_unit : 0) : $free_unit;
		$history = (object)[
			'pay' => $pay,
			'free' => $free
		];
		if($remain > 0) {
			return $this->quaterCalculate($remain, $rule_unit, $free_unit, $history, $type);
		}
		return $history;
	}

	private function getScanTotal($scan_result = [])
	{
		$total = 0;
		foreach ($scan_result as $key => $row) {
			$total += $row->total;
		}
		return $total;
	}

	private function pricingRuleCalculate($scan_result)
	{
		$final_scan_result = [];
		$pricing_rule = $this->pricing_rule;
		$FREE = [];

			foreach ($pricing_rule->rules as $key_rule => $row_rule) {
				$RULE_SKU = $row_rule->sku;
				$RULE_TYPE = $row_rule->type;
				
				if ($RULE_TYPE == 'free_same_item') {
					$RULE_FREE_ITEM_SKU = $row_rule->config->free_item_sku;
					$RULE_UNIT = $row_rule->config->unit;
					$RULE_FREE_UNIT = $row_rule->config->free_unit;
					$FREE_PRODUCT = $this->getProductPrice($row_rule->config->free_item_sku);
					$SCAN_ITEM = !empty($scan_result[$RULE_SKU]) ? $scan_result[$RULE_SKU] : [];
					$SCAN_ITEM_FREE = !empty($scan_result[$RULE_FREE_ITEM_SKU]) ? $scan_result[$RULE_FREE_ITEM_SKU] : [];
					if (!empty($SCAN_ITEM)) {
						if ($SCAN_ITEM->quantity >= $RULE_UNIT) {
							$QUATER_RESULT = $this->quaterCalculate($SCAN_ITEM->quantity, $RULE_UNIT, $RULE_FREE_UNIT, [], 'same');
							if($RULE_UNIT < $SCAN_ITEM->quantity) {
								$SCAN_ITEM->quantity = $QUATER_RESULT->pay;
								$SCAN_ITEM->total = $QUATER_RESULT->pay * $SCAN_ITEM->price;
							}
							$FREE[] = [
								'type' => 'free_same_item',
								'item' => $FREE_PRODUCT->sku,
								'quantity' => $QUATER_RESULT->free, 
								'price' => $FREE_PRODUCT->price,
								'total' => $QUATER_RESULT->free * $FREE_PRODUCT->price
							];
						}
					}
				}
				if ($RULE_TYPE == 'free_different_item') {
					$RULE_FREE_ITEM_SKU = $row_rule->config->free_item_sku;
					$RULE_UNIT = $row_rule->config->unit;
					$RULE_FREE_UNIT = $row_rule->config->free_unit;
					$FREE_PRODUCT = $this->getProductPrice($row_rule->config->free_item_sku);
					$SCAN_ITEM = !empty($scan_result[$RULE_SKU]) ? $scan_result[$RULE_SKU] : [];
					$SCAN_ITEM_FREE = !empty($scan_result[$RULE_FREE_ITEM_SKU]) ? $scan_result[$RULE_FREE_ITEM_SKU] : [];
					if (!empty($SCAN_ITEM)) {
						if ($SCAN_ITEM->quantity >= $RULE_UNIT) {
							$QUATER_RESULT = $this->quaterCalculate($SCAN_ITEM->quantity, $RULE_UNIT, $RULE_FREE_UNIT, [], 'diffent');
							if($RULE_UNIT <= $SCAN_ITEM->quantity) {
								$SCAN_ITEM_FREE->quantity = $SCAN_ITEM_FREE->quantity - $QUATER_RESULT->free;
								$SCAN_ITEM_FREE->total = $SCAN_ITEM_FREE->quantity * $SCAN_ITEM_FREE->price;
								if ($SCAN_ITEM_FREE->quantity <= 0) { 
									unset($scan_result[$RULE_FREE_ITEM_SKU]);
								}
							}
							$FREE[] = [
								'type' => 'free_different_item',
								'item' => $FREE_PRODUCT->sku,
								'quantity' => $QUATER_RESULT->free, 
								'price' => $FREE_PRODUCT->price,
								'total' => $QUATER_RESULT->free * $FREE_PRODUCT->price
							];
						}
					}
				}
				if ($RULE_TYPE == 'discount_same_item_price') {
					$RULE_DISCOUNT_ACCOUNT =  $row_rule->config->discount_amount;
					$RULE_UNIT = $row_rule->config->unit;
					$ORGINAL_PRODUCT = $this->getProductPrice($row_rule->sku);
					$SCAN_ITEM = !empty($scan_result[$RULE_SKU]) ? $scan_result[$RULE_SKU] : [];
					if (!empty($SCAN_ITEM)) {
						if ($SCAN_ITEM->quantity >= $RULE_UNIT) {
							$SCAN_ITEM->price = $RULE_DISCOUNT_ACCOUNT;
							$SCAN_ITEM->total = $SCAN_ITEM->quantity * $RULE_DISCOUNT_ACCOUNT;
							$FREE[] = [
								'type' => 'discount_same_item_price',
								'item' => $SCAN_ITEM->sku,
								'quantity' => $SCAN_ITEM->quantity, 
								'original_price' => $ORGINAL_PRODUCT->price,
								'discount_price' => $SCAN_ITEM->price,
								'total' => $SCAN_ITEM->quantity * $RULE_DISCOUNT_ACCOUNT,
								'total_discount' => ($SCAN_ITEM->quantity  * $ORGINAL_PRODUCT->price) - ($SCAN_ITEM->quantity * $RULE_DISCOUNT_ACCOUNT)
							];
						}
					}
				}

				$total = $this->getScanTotal($scan_result);

				$final_scan_result = [
					'items' => $scan_result,
					'free' => $FREE,
					'total' => $total
				];
			}

		$this->final_scan_result[] = $final_scan_result;
	}

	public function scan($action = [])
	{
		$class_item = $this->classItem($action->items);
		$scan_result = $this->scanResult($class_item);
		$final_scan_result = $this->pricingRuleCalculate($scan_result);
	}

	public function total()
	{
		return $this->final_scan_result;
	}
}