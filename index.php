<?php
require_once ('modules/Checkout.php');
require_once ('models/PricingRule.php');

class Main
{
	function __construct()
	{

	}

	public static function index()
	{
		$scenarios = json_decode(file_get_contents('json/scenarios.json'));
		$pricing_rule = [];
		$pricing_rule = new PricingRule();
		$pricing_rule = $pricing_rule
							->where('status', 1) //active rules
							->first();
		$checkout = new Checkout($pricing_rule);
		foreach ($scenarios as $key_scena => $row_scena) {
			$scan = $checkout->scan($row_scena);
		}
		$result = $checkout->total();
		print_r($result);
	}
}
	
Main::index();

