<?php
class BaseModel
{

	private $db = [];
	private $where = [];

	function __construct($table)
	{
		$this->db = json_decode(file_get_contents($table));
	}

	public function where($filed = '', $value = '')
	{
		$this->where[] = (object)[
			'field' => $filed, 
			'value' => $value
		];
		return $this;
	}

	public function first()
	{
		$orginal_data = $this->db;
		$new_data = [];
		if (!empty($this->where)) {
			$this->db = [];
			foreach ($orginal_data as $key_db => $row_db) {
				foreach ($this->where as $key_where => $row_where) {
					$field = $row_where->field;
					$value = $row_where->value;
					if(!empty($row_db->$field)) {
						if ($row_db->$field == $value) {
							$new_data[] = (object)$row_db;
						}
					}
				}
			}			
		}

		$this->db = !empty($new_data) ? $new_data : $this->db;
		return $this->db[0];
	}

	public function get()
	{
		$orginal_data = $this->db;
		$new_data = [];
		if (!empty($this->where)) {
			$this->db = [];
			foreach ($orginal_data as $key_db => $row_db) {
				foreach ($this->where as $key_where => $row_where) {
					$field = $row_where->field;
					$value = $row_where->value;
					if(!empty($row_db->$field)) {
						if ($row_db->$field == $value) {
							$new_data[] = (object)$row_db;
						}
					}
				}
			}			
		}

		$this->db = !empty($new_data) ? $new_data : $this->db;
		return $this->db;
	}
}